package com.kshrd.recyclerviewdemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

public class MyListAdapter extends RecyclerView.Adapter<MyListAdapter.ViewHoder> {

    public AnimalModel[] animalList;

    public MyListAdapter(AnimalModel[] animalList) {
        this.animalList = animalList;
    }

    @NonNull
    @Override
    public ViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.my_list,parent,false);
        ViewHolder viewHolder = new ViewHoder(listItem);

        return (ViewHoder) viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHoder holder, int position) {
        AnimalModel animalModel = animalList[position];

        holder.imageView.setImageResource(animalModel.getImage());
        holder.tvName.setText(animalModel.getName());
        holder.tvDescription.setText(animalModel.getDescription());

    }

    @Override
    public int getItemCount() {
        return animalList.length;
    }

     class ViewHoder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView tvName,tvDescription;
        LinearLayout linearLayout;

         public ViewHoder(@NonNull View itemView) {
             super(itemView);
             imageView = itemView.findViewById(R.id.imageView);
             tvName = itemView.findViewById(R.id.tvName);
             tvDescription = itemView.findViewById(R.id.tvDescription);
             linearLayout = itemView.findViewById(R.id.mySubList);
         }
     }
}
